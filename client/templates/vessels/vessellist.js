Template.vessellist.helpers({
    maybeSelected: function() {
        var currentRoute = Router.current();
        return currentRoute && this._id === currentRoute.params._id ? 'btn-primary' : 'btn-default';
    }
});
