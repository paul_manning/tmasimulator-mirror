Vessels = Meteor.subscribe('vessels');
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notfound',
    title: 'TMA Simulator'
});
Router.route('home', {
    path: '/',
    title: 'TMA Simulator: Home Page'
});
Router.route('vessels', {
    data: function() {
        return {
            vesselList: Vessels.find(),
            selectedVessel: {}
        }
    },
    title: 'TMA Simulator: Vessel List'

});
Router.route('vessel', {
    path: 'vessel/:_id',
    layoutTemplate: 'layout',
    data: function(){
        return {
            vesselList: Vessels.find(),
            selectedVessel: Vessels.findOne({_id: this.params._id})
        }
    },
    template: 'vessels',
    title: function() {
        return 'TMA Simulator: ' + this.data().selectedVessel.name;
    }

});

Router.route('simulation', {
    layoutTemplate: 'layout',
    title: 'TMA Simulator: Simulation'
});
Router.route('about', {
    layoutTemplate: 'layout',
    title: 'TMA Simulator: About Page'

});
Router.route('tracks', {
    layoutTemplate: 'layout',
    title: 'TMA Simulator: Tracks'

});

var requireLogin = function() {
    if (!Meteor.user()) {
        this.render('home');
    } else {
        this.next();
    }
};

Router.onBeforeAction(requireLogin, {
    except: ['home', 'about']
});
