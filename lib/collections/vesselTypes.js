vesselTypes = new Meteor.Collection('vesselTypes');

if (Meteor.isServer) {
    Meteor.startup( function() {
        if (!vesselTypes.findOne()) {
            for( i=0; i<5; i++) {
                vesselTypes.insert({
                    name: 'Sample Vessel Type '+i,
                    isExample: true,
                    owner: 'pmanning'
                })
            }
        }
    })
}