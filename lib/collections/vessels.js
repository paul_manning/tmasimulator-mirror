Vessels = new Meteor.Collection('vessels');

if (Meteor.isServer) {
    Meteor.startup( function() {
        if (!Vessels.findOne()) {
            for( i=0; i<5; i++) {
                Vessels.insert({
                    name: 'Sample Vessel '+i,
                    isExample: true,
                    owner: 'pmanning',
                    vesselTypeId: null
                })
            }
        }
    })
}